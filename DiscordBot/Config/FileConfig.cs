﻿using System;
using Newtonsoft.Json;

namespace Bot.Config
{
    class FileConfig : IDisposable
    {
        private bool _disposed = false;

        [JsonProperty("token")]
        public string Token { get; private set; }

        public void Dispose()
        {
            Token = default;
            GC.SuppressFinalize(this);
        }
    }
}