﻿#pragma warning disable IDE0040
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Bot.Interaction
{
    class CollectClientFunctions
    {
        List<Appendable> List { get; }

        public CollectClientFunctions()
        {
            List = new List<Appendable>();
        }

        public async Task Create()
        {
            await Task.Run(() =>
            {
                List.Add(new CuteBotResponse());
            });
        }

        public async Task Append()
        {
            await Task.Run(() =>
            {
                foreach (var func in List)
                    AddEvent.ClientFunctions += func.Add;
            });
        }
    }
}
