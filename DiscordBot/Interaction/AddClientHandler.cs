﻿#pragma warning disable IDE0040
using System;
using System.Linq;
using System.Collections.Generic;

using DSharpPlus;

namespace Bot.Interaction
{
    public delegate void AddClientHandler(DiscordClient d);

    static class AddEvent
    {
        static List<AddClientHandler> list = new List<AddClientHandler>();

        public static event AddClientHandler ClientFunctions {
            add => list.Add(value);
            remove => throw new NotSupportedException("Cannot remove client functions from event.");
        }

        public static void Populate(DiscordClient d)
        {
            var handlers = list.ToArray();

            if (!handlers.Any())
                return;

            foreach (var handler in handlers)
                handler(d);
        }
    }
}