﻿using DSharpPlus;

namespace Bot.Interaction
{
    abstract class Appendable
    {
        public abstract void Add(DiscordClient d);
    }
}
