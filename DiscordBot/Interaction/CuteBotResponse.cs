﻿#pragma warning disable IDE0040
using DSharpPlus;

using Bot;

namespace Bot.Interaction
{
    class CuteBotResponse : Appendable
    {
        public override void Add(DiscordClient d)
        {
            d.MessageCreated += async e =>
            {
                if (e.Message.Content.ToLower().StartsWith("ping"))
                    await e.Message.RespondAsync("pong!");
            };
        }
    }
}
