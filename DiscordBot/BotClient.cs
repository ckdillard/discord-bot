﻿using System;
using System.Threading.Tasks;

using DSharpPlus;
using DSharpPlus.CommandsNext;

using Bot.Commands;
using Bot.Interaction;

namespace Bot
{
    class BotClient
    {
        public DiscordClient Discord { get; }
        public CommandsNextModule Commands { get; }

        public BotClient(string token)
        {
            Discord = new DiscordClient(new DiscordConfiguration
            {
                Token = token,
                TokenType = TokenType.Bot,
                UseInternalLogHandler = true,
                LogLevel = LogLevel.Debug,
            });

            Commands = Discord.UseCommandsNext(new CommandsNextConfiguration
            {
                StringPrefix = ";"
            });

            Commands.RegisterCommands<TestCommand>();
        }

        public async Task RunAsync(string[] args)
        {
            var collect = new CollectClientFunctions();
            Task a = collect.Create();
            a.Wait();

            Task b = collect.Append();
            b.Wait();

            AddEvent.Populate(Discord);

            await Discord.ConnectAsync();
            await Task.Delay(-1);
        }
    }
}
