﻿#pragma warning disable IDE0040
using System.Threading.Tasks;

using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;

namespace Bot.Commands
{
    class TestCommand
    {
        [Command("hi")]
        public async Task Hi(CommandContext ctx)
        {
            await ctx.RespondAsync($"Hi, {ctx.User.Mention}!");
        }
    }
}
