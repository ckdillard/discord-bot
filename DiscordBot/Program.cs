﻿using System;
using System.IO;
using System.Threading.Tasks;

using DSharpPlus;
using DSharpPlus.CommandsNext;

using Newtonsoft.Json;

using Bot.Config;
using Bot.Commands;

namespace Bot
{
    class Program
    {
        /// <summary>
        /// Starts the MainAsync task.
        /// </summary>
        /// <param name="args">Console arguments.</param>
        static void Main(string[] args)
        {
            var filePath = Path.GetFullPath(Path.Combine(AppContext.BaseDirectory, "../../../", "bot.json"));
            var config = JsonConvert.DeserializeObject<FileConfig>(File.ReadAllText(filePath));
            var bot = new BotClient(config.Token);

            config.Dispose();

            bot.RunAsync(args).ConfigureAwait(false).GetAwaiter().GetResult();
        }
    }
}
